# marin_lnmp

#### 介绍
Lnmp开发环境

#### 软件架构
基于docker和docker_compose,支持php redis mysql nginx,可以自行修改安装


#### 安装教程

```# git clone https://gitee.com/marins/marin_lnmp.git```

```# cd marin_lnmp```

```# mkdir -p ${PWD}/mysql/data```

```mkdir -p ${PWD}/redis/data```

#### 使用说明

1.  mysql密码root,可以自行更改
2.  redis默认不需要auth
3.  php版本：```7.1.30```
4.  mysql版本：```5.6```

### nginx server配置
```
server {
        listen   80;
        server_name  cs.info.com;
        #root是nginx项目根路径地址
        set $root /data/website;
        root $root;

        access_log  /data/website/access.log;
        error_log /data/website/error.log;
        index  index.php index.html index.htm;

       location ~ \.php$ {
           try_files $uri =404;
           fastcgi_pass  php:9000;
           fastcgi_index index.php;
           #切记SCRIPT_FILENAME 需要填写php容器里边的php项目文件路径,否则会出现No file inputed的错误
           fastcgi_param SCRIPT_FILENAME /data/website/$fastcgi_script_name;
           include        fastcgi_params;
       }
    }
```
###注意事项
一定切记docker-compose的nginx服务还有php服务为了不出错，都需要挂载php项目目录